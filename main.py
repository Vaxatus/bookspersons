from Data import book_list
from Data import Book
from Data import Osoba
from Data import Person
import random


def print_book(book):
    print(book.name)
    print(book.author)
    print(book.isbn)

def generate_books():
    books = []
    while len(books) < 10:
        num = len(books)
        random.seed(len(books) + 1)
        name_data = book_list[num][1].split(" ")
        # print(name_data)
        book = Book(book_list[num][0], Person(name_data[0], name_data[1]), random.randint(1000000000000, 9999999999999))
        # book = Book(book_list[num][0], book_list[num][1], random.randint(1000000000000, 9999999999999))
        # print_book(book)
        books.append(book)
    return books

if __name__ == "__main__":
    books = generate_books()

    osoba1 = Osoba("Jacek", "Ziom")
    osoba2 = Osoba("Ewa", "Danko")

    print("czy mozesz kupic alk osoba1 " + str(osoba1.can_i_buy_alcohol()))
    osoba2.set_age(44)
    print("czy mozesz kupic alk osoba2 " + str(osoba2.can_i_buy_alcohol()))
    print(osoba1.name)
    print(osoba1.get_liked_books())

    print(osoba2.name)
    print(osoba2.get_liked_books())

    osoba2.add_book(books[5])
    osoba2.add_book(books[2])
    osoba2.add_book(books[1])
    for book in osoba2.get_liked_books():
        print("**************")
        print_book(book)
        print("**************")

    print("=================")

    osoba2.del_book(books[2])
    for book in osoba2.get_liked_books():
        print("**************")
        print_book(book)
        print("**************")