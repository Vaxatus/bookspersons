book_list = [
    ["Angole", "Ewa Winnicka"],
    ["Astrid Lindgren. Opowieść o życiu i twórczości", "Margareta Strömstedt"],
    ["Awantury na tle powszechnego ciążenia", "Tomasz Lem"],
    ["Bajka o Rašce i inne reportaże sportowe", "Ota Pavel"],
    ["Chłopczyce z Kabulu. Za kulisami buntu obyczajowego w Afganistanie", "Jenny Nordberg"],
    ["Czyje jest nasze życie? Olga Drenda", "Bartłomiej Dobroczyński"],
    ["Detroit. Sekcja zwłok Ameryki", "Charlie LeDuff"],
    ["Głód", "Martín Caparrós"],
    ["Hajstry. Krajobraz bocznych dróg", "Adam Robiński"],
    ["Jak rozmawiać o książkach", "których się nie czytało", "Pierre Bayard"],
    ["J jak jastrząb", "Helen Macdonald"],
    ["Łódź 370", "Annah Björk", "Mattias Beijmo"],
    ["Matka młodej matki", "Justyna Dąbrowska"],
    ["Mężczyźni objaśniają mi świat", "Rebecca Solnit"],
    ["Nie ma się czego bać", "Justyna Dąbrowska"],
    ["Non/fiction. Nieregularnik reporterski"],
    ["Obwód głowy", "Włodzimierz Nowak"],
    ["Ostatnie dziecko lasu", "Richard Louv"],
    ["Polska odwraca oczy", "Justyna Kopińska"],
    ["Powrócę jako piorun", "Maciej Jarkowiec"],
    ["Simona. Opowieść o niezwyczajnym życiu Simony Kossak", "Anna Kamińska"],
    ["Szlaki. Opowieści o wędrówkach", "Robert Macfarlane"],
    ["Wykluczeni", "Artur Domosławski"]
]
class Person:
    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

class Osoba(Person):
    age = 0

    def __init__(self, name, surname):
        super().__init__(name, surname)
        self.liked_books = []

    def set_age(self, age): # setter
        self.age = age

    def can_i_buy_alcohol(self):
        return self.age >= 18

    def add_book(self, book):
        self.liked_books.append(book)

    def add_book_by_name(self, name, author, isbn):
        self.add_book(Book(name, author, isbn))

    def del_book(self, book):
        self.liked_books.remove(book)

    def del_book_by_name(self, name):
        deleted = False
        for book in self.liked_books:
            if name == book.name:
                self.del_book(book)
                deleted = True
                break
        if not deleted:
            for book in self.liked_books:
                if name == book.author:
                    self.del_book(book)
                    break

    def get_liked_books(self):
        return self.liked_books

class Book:
    def __init__(self, name, author, isbn):
        self.name = name
        self.author = author
        self.isbn = isbn